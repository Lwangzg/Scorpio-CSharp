//github : https://github.com/qingfeng346/Scorpio-CSharp
namespace Scorpio {
    public static class Version {
        public const string version = "2.2.0";
        public const string date = "2021-07-27";
    }
}
